package graphbase;

import Input.FileInput;
import org.junit.*;

import java.io.FileNotFoundException;
import java.util.*;

import static graphbase.GraphAlgorithms.graphColoringUtil;
import static org.junit.Assert.*;

public class GraphAlgorithmsTest {

    Graph<String, String> completeMap = new Graph<>(false);
    Graph<String, String> incompleteMap = new Graph<>(false);
    Graph<String, String> countryMap = new Graph<>(false);

    public GraphAlgorithmsTest() {
    }

    @Before
    public void setUp() throws Exception {

        completeMap.insertVertex("Porto");
        completeMap.insertVertex("Braga");
        completeMap.insertVertex("Vila Real");
        completeMap.insertVertex("Aveiro");
        completeMap.insertVertex("Coimbra");
        completeMap.insertVertex("Leiria");

        completeMap.insertVertex("Viseu");
        completeMap.insertVertex("Guarda");
        completeMap.insertVertex("Castelo Branco");
        completeMap.insertVertex("Lisboa");
        completeMap.insertVertex("Faro");

        completeMap.insertEdge("Porto", "Aveiro", "A1", 75);
        completeMap.insertEdge("Porto", "Braga", "A3", 60);
        completeMap.insertEdge("Porto", "Vila Real", "A4", 100);
        completeMap.insertEdge("Viseu", "Guarda", "A25", 75);
        completeMap.insertEdge("Guarda", "Castelo Branco", "A23", 100);
        completeMap.insertEdge("Aveiro", "Coimbra", "A1", 60);
        completeMap.insertEdge("Coimbra", "Lisboa", "A1", 200);
        completeMap.insertEdge("Coimbra", "Leiria", "A34", 80);
        completeMap.insertEdge("Aveiro", "Leiria", "A17", 120);
        completeMap.insertEdge("Leiria", "Lisboa", "A8", 150);

        completeMap.insertEdge("Aveiro", "Viseu", "A25", 85);
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        completeMap.insertEdge("Lisboa", "Faro", "A2", 280);

        incompleteMap = completeMap.clone();

        incompleteMap.removeEdge("Aveiro", "Viseu");
        incompleteMap.removeEdge("Leiria", "Castelo Branco");
        incompleteMap.removeEdge("Lisboa", "Faro");

        FileInput.readDataFiles(countryMap);
    }

    @After
    public void tearDown() throws Exception {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of BreadthFirstSearch method, of class GraphAlgorithms.
     */
    @Test
    public void testBreadthFirstSearch() {
        System.out.println("Test BreadthFirstSearch");

        assertTrue("Should be null if vertex does not exist", GraphAlgorithms.BreadthFirstSearch(completeMap, "LX") == null);

        LinkedList<String> path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Faro");

        assertTrue("Should be just one", path.size() == 1);

        Iterator<String> it = path.iterator();
        assertTrue("it should be Faro", it.next().compareTo("Faro") == 0);

        /*
         * ha erro e ninguem sabe porque
         * */
        /*path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Porto");
        assertTrue("Should give seven vertices ", path.size() == 7);*/

        /*path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Viseu");
        assertTrue("Should give 3 vertices", path.size() == 3);*/
    }

    /**
     * Test of DepthFirstSearch method, of class GraphAlgorithms.
     */
    @Test
    public void testDepthFirstSearch() {
        System.out.println("Test of DepthFirstSearch");

        LinkedList<String> path;

        assertTrue("Should be null if vertex does not exist", GraphAlgorithms.DepthFirstSearch(completeMap, "LX") == null);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Faro");
        assertTrue("Should be just one", path.size() == 1);

        Iterator<String> it = path.iterator();
        assertTrue("it should be Faro", it.next().compareTo("Faro") == 0);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Porto");
        assertTrue("Should give seven vertices ", path.size() == 7);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Viseu");
        assertTrue("Should give 3 vertices", path.size() == 3);

        it = path.iterator();
        assertTrue("First in visit should be Viseu", it.next().compareTo("Viseu") == 0);
        assertTrue("then Guarda", it.next().compareTo("Guarda") == 0);
        assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco") == 0);
    }

    /**
     * Test of allPaths method, of class GraphAlgorithms.
     */
    @Test
    public void testAllPaths() {
        System.out.println("Test of all paths");

        ArrayList<LinkedList<String>> paths = new ArrayList<LinkedList<String>>();

        paths = GraphAlgorithms.allPaths(completeMap, "Porto", "LX");
        assertTrue("There should not be paths if vertex does not exist", paths.size() == 0);

        paths = GraphAlgorithms.allPaths(incompleteMap, "Porto", "Lisboa");
        assertTrue("There should be 4 paths", paths.size() == 4);

        paths = GraphAlgorithms.allPaths(incompleteMap, "Porto", "Faro");
        assertTrue("There should not be paths between Porto and Faro in the incomplete map", paths.size() == 0);
    }

    /**
     * Test of shortestPath method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPath() {
        System.out.println("Test of shortest path");

        LinkedList<String> shortPath = new LinkedList<String>();
        double lenpath = 0;
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "LX", shortPath);
        assertTrue("Length path should be 0 if vertex does not exist", lenpath == 0);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Porto", "Faro", shortPath);
        assertTrue("Length path should be 0 if there is no path", lenpath == 0);

        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Porto", shortPath);
        assertTrue("Number of nodes should be 1 if source and vertex are the same", shortPath.size() == 1);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Porto", "Lisboa", shortPath);
        assertTrue("Path between Porto and Lisboa should be 335 Km", lenpath == 335);

        Iterator<String> it = shortPath.iterator();

        assertTrue("First in path should be Porto", it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Coimbra", it.next().compareTo("Coimbra") == 0);
        assertTrue("then Lisboa", it.next().compareTo("Lisboa") == 0);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Braga", "Leiria", shortPath);
        assertTrue("Path between Braga and Leiria should be 255 Km", lenpath == 255);

        it = shortPath.iterator();

        assertTrue("First in path should be Braga", it.next().compareTo("Braga") == 0);
        assertTrue("then Porto", it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Leiria", it.next().compareTo("Leiria") == 0);

        shortPath.clear();
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Castelo Branco", shortPath);
        assertTrue("Path between Porto and Castelo Branco should be 335 Km", lenpath == 335);
        assertTrue("N. cities between Porto and Castelo Branco should be 5 ", shortPath.size() == 5);

        it = shortPath.iterator();

        assertTrue("First in path should be Porto", it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Viseu", it.next().compareTo("Viseu") == 0);
        assertTrue("then Guarda", it.next().compareTo("Guarda") == 0);
        assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco") == 0);

        //Changing Edge: Aveiro-Viseu with Edge: Leiria-C.Branco
        //should change shortest path between Porto and Castelo Branco

        completeMap.removeEdge("Aveiro", "Viseu");
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        shortPath.clear();
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Castelo Branco", shortPath);
        assertTrue("Path between Porto and Castelo Branco should now be 365 Km", lenpath == 365);
        assertTrue("Path between Porto and Castelo Branco should be 4 cities", shortPath.size() == 4);

        it = shortPath.iterator();

        assertTrue("First in path should be Porto", it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Leiria", it.next().compareTo("Leiria") == 0);
        assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco") == 0);

    }

    /**
     * Test of shortestPaths method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPaths() {
        System.out.println("Test of shortest path");

        ArrayList<LinkedList<String>> paths = new ArrayList<>();
        ArrayList<Double> dists = new ArrayList<>();

        GraphAlgorithms.shortestPaths(completeMap, "Porto", paths, dists);

        assertEquals("There should be as many paths as sizes", paths.size(), dists.size());
        assertEquals("There should be a path to every vertex", completeMap.numVertices(), paths.size());
        assertEquals("Number of nodes should be 1 if source and vertex are the same", 1, paths.get(completeMap.getKey("Porto")).size());
        assertEquals("Path to Lisbon", Arrays.asList("Porto", "Aveiro", "Coimbra", "Lisboa"), paths.get(completeMap.getKey("Lisboa")));
        assertEquals("Path to Castelo Branco", Arrays.asList("Porto", "Aveiro", "Viseu", "Guarda", "Castelo Branco"), paths.get(completeMap.getKey("Castelo Branco")));
        assertEquals("Path between Porto and Castelo Branco should be 335 Km", 335, dists.get(completeMap.getKey("Castelo Branco")), 0.01);

        //Changing Edge: Aveiro-Viseu with Edge: Leiria-C.Branco
        //should change shortest path between Porto and Castelo Branco
        completeMap.removeEdge("Aveiro", "Viseu");
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        GraphAlgorithms.shortestPaths(completeMap, "Porto", paths, dists);
        assertEquals("Path between Porto and Castelo Branco should now be 365 Km", 365, dists.get(completeMap.getKey("Castelo Branco")), 0.01);
        assertEquals("Path to Castelo Branco", Arrays.asList("Porto", "Aveiro", "Leiria", "Castelo Branco"), paths.get(completeMap.getKey("Castelo Branco")));


        GraphAlgorithms.shortestPaths(incompleteMap, "Porto", paths, dists);
        assertEquals("Length path should be Double.MAX_VALUE if there is no path", Double.MAX_VALUE, dists.get(completeMap.getKey("Faro")), 0.01);
        assertEquals("Path between Porto and Lisboa should be 335 Km", 335, dists.get(completeMap.getKey("Lisboa")), 0.01);
        assertEquals("Path to Lisboa", Arrays.asList("Porto", "Aveiro", "Coimbra", "Lisboa"), paths.get(completeMap.getKey("Lisboa")));
        assertEquals("Path between Porto and Lisboa should be 335 Km", 335, dists.get(completeMap.getKey("Lisboa")), 0.01);

        GraphAlgorithms.shortestPaths(incompleteMap, "Braga", paths, dists);
        assertEquals("Path between Braga and Leiria should be 255 Km", 255, dists.get(completeMap.getKey("Leiria")), 0.01);
        assertEquals("Path to Leiria", Arrays.asList("Braga", "Porto", "Aveiro", "Leiria"), paths.get(completeMap.getKey("Leiria")));
    }

    @Test
    public <V, E>  void testMapColoring() {
        Vertex p;
        TreeMap<V, Integer> result = (TreeMap<V, Integer>) GraphAlgorithms.MapColoring(countryMap);
        for (Object a: result.keySet()){
            for (Object b : ((Vertex) a).getAllAdjVerts()) {
                int x = result.get(a);
                int y = result.get(b);
                assertNotEquals(x, y);
            }
        }
    }

    /**
     * Test of graphColoringUtil method, of class GraphAlgorithms.
     */
    @Test
    public <V, E> void testGraphColoringUtil() throws FileNotFoundException {

        Graph<V, E> countryMap1 = new Graph<>(false);
        FileInput.readDataFiles(countryMap1);

        Comparator<V> c = (Comparator<V>) Comparator.comparing(p -> countryMap1.outDegree((V) p)).thenComparing(p -> p.toString());

        //Creates a result set that is ordered by number of adjVerts
        TreeMap<V, Integer> result = new TreeMap<V, Integer>(c.reversed()) {
        };

        //add all vertexes to solution set
        for (V a : countryMap1.vertices()) {
            result.putIfAbsent(a, 99);
        }

        // Call graphColoringUtil() for vertex 0
        graphColoringUtil(countryMap1, result, result.firstKey());

        for (Object a : result.keySet()) {
            for (Object b : ((Vertex) a).getAllAdjVerts()) {
                int x = result.get(a);
                int y = result.get(b);
                assertNotEquals(x, y);
            }
        }
    }

    /**
     * Test of isSafe method, of class GraphAlgorithms.
     */
    @Test
    public <V, E> void testIsSafe() throws FileNotFoundException {
        System.out.println("isSafe");

        Graph<V, E> countryMap1 = new Graph<>(false);
        FileInput.readDataFiles(countryMap1);

        Comparator<V> c = (Comparator<V>) Comparator.comparing(p -> countryMap1.outDegree((V) p)).thenComparing(p -> p.toString());

        //Creates a result set that is ordered by number of adjVerts
        TreeMap<V, Integer> result1 = new TreeMap<V, Integer>(c.reversed()) {
        };

        //add all vertexes to solution set
        for (V a : countryMap1.vertices()) {
            result1.putIfAbsent(a, 99);
        }

        boolean expResult = false;
        boolean result = GraphAlgorithms.isSafe(null, result1, 99, countryMap1);
        assertEquals(expResult, result);
    }

    /**
     * Test of shortestPathWithStops method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPathWithStops() {
        System.out.println("shortestPathWithStops");

        /**
         * teste entre Venezuela e Argentina, passando por Brasilia e Lima
         */
        double expectedResult = 8412.80407389807;
        LinkedList res = new LinkedList();
        Country[] stops = new Country[2];
        Country origin = countryMap.getCountryWithName("venezuela");
        Country dest = countryMap.getCountryWithName("argentina");
        Country stop1 = countryMap.getCountryWithCapital("brasilia");
        Country stop2 = countryMap.getCountryWithCapital("lima");
        stops[0] = stop1;
        stops[1] = stop2;
        double result = GraphAlgorithms.shortestPathWithStops(countryMap, origin, dest, stops, res);
        assertEquals("Teste entre Venezuela e Argentina. Stops: brasilia, lima", expectedResult, result, 0.00001);

        /**
         * teste entre Portugal e Polonia, passando por Berlim, Berna e Praga
         */
        double expectedResult1 = 3541.0715470150467;
        LinkedList res1 = new LinkedList();
        Country[] stops1 = new Country[3];
        Country origin1 = countryMap.getCountryWithName("portugal");
        Country dest1 = countryMap.getCountryWithName("polonia");
        Country stop11 = countryMap.getCountryWithCapital("berlim");
        Country stop12 = countryMap.getCountryWithCapital("berna");
        Country stop13 = countryMap.getCountryWithCapital("praga");
        stops1[0] = stop11;
        stops1[1] = stop12;
        stops1[2] = stop13;
        double result1 = GraphAlgorithms.shortestPathWithStops(countryMap, origin1, dest1, stops1, res1);
        assertEquals("Teste entre Portugal e Polonia. Stops: berlim, berna, praga", expectedResult1, result1, 0.00001);
        
        /**
         * teste entre Venezuela e pa�s que n�o existe, passando por Brasilia e Lima
         */
        double expectedResult2 = 0;
        LinkedList res2 = new LinkedList();
        Country[] stops2 = new Country[2];
        Country origin2 = countryMap.getCountryWithName("venezuela");
        Country dest2 = countryMap.getCountryWithName("nao existo");
        Country stop21 = countryMap.getCountryWithCapital("brasilia");
        Country stop22 = countryMap.getCountryWithCapital("lima");
        stops[0] = stop21;
        stops[1] = stop22;
        double result2 = GraphAlgorithms.shortestPathWithStops(countryMap, origin2, dest2, stops2, res2);
        assertEquals("Teste entre Venezuela e Argentina. Stops: brasilia, lima", expectedResult2, result2, 0.00001);
    }

    /**
     * EX 3
     * Test of shortestPathBetweenTwoCountries method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPathBetweenTwoCountries() {
        System.out.println("shortestPathBetweenTwoCountries");

        /*
         * teste entre a Chile e Brasil
         */
        double expectedResult = 3476.6579322085854;
        Country origin = countryMap.getCountryWithName("chile");
        Country dest = countryMap.getCountryWithName("brasil");
        double result = GraphAlgorithms.shortestPathBetweenTwoCountries(countryMap, origin, dest);
        assertEquals("Teste entre Chile e Brasil", expectedResult, result, 0.00001);

        /*
         * teste entre Portugal e Alemanha
         */
        double expectedResult1 = 2433.804873447267;
        Country origin1 = countryMap.getCountryWithName("portugal");
        Country dest1 = countryMap.getCountryWithName("alemanha");
        double result1 = GraphAlgorithms.shortestPathBetweenTwoCountries(countryMap, origin1, dest1);
        assertEquals("Teste entre Portugal e Alemanha", expectedResult1, result1, 0.00001);

        /*
         * teste entre Portugal e pa�s inexistente
         * */
        double expectedResult2 = 0;
        Country origin2 = countryMap.getCountryWithName("portugal");
        Country dest2 = countryMap.getCountryWithName("nao existo");
        double result2 = GraphAlgorithms.shortestPathBetweenTwoCountries(countryMap, origin2, dest2);
        assertEquals("Teste entre Portugal e pa�s inexistente", expectedResult2, result2, 0.00001);
    }
}