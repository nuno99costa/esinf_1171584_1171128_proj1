package graphbase;

public class Border {

    private Country cont1;
    private Country cont2;

    public Border(Country cont1, Country cont2) {
        this.cont1 = cont1;
        this.cont2 = cont2;
    }

    public Border() {
        this.cont1 = null;
        this.cont2 = null;
    }

    /**
     * Returns first country that forms the border
     * @return first country as Country object
     */
    public Country getCountry1() {
        return cont1;
    }

    /**
     * Sets new first country to form the border
     * @param cont1 new country to set as Country object
     */
    public void setCountry1(Country cont1) {
        this.cont1 = cont1;
    }

    /**
     * Returns second country that forms the border
     * @return second country as Country object
     */
    public Country getCountry2() {
        return cont2;
    }

    /**
     * Sets new second country to form the border
     * @param cont2 new country to set as Country object
     */
    public void setCountry2(Country cont2) {
        this.cont2 = cont2;
    }

}
