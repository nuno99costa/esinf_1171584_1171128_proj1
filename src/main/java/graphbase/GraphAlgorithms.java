package graphbase;

import java.util.*;

/**
 * @author DEI-ESINF
 */

public class GraphAlgorithms {

//region Ex2

    /**
     * Color graph by number
     *
     * @param g   map to color -used abstract classes provided in ESINF class
     * @param <V> Abstract Vertex
     * @param <E> Abstract Edge
     * @return neighbor-ordered treemap with every vertex in g as keys and their respective 'colors' as values.
     */
    public static <V, E> TreeMap<V, Integer> MapColoring(Graph<V, E> g) {
        Comparator<V> c = (Comparator<V>) Comparator.comparing(p -> g.outDegree((V) p)).thenComparing(p -> p.toString());

        //Creates a result set that is ordered by number of adjVerts
        TreeMap<V, Integer> result = new TreeMap<V, Integer>(c.reversed()) {
        };

        //add all vertexes to solution set
        for (V a : g.vertices()) {
            result.putIfAbsent(a, 99);
        }

        // Call graphColoringUtil() for vertex 0
        if (!graphColoringUtil(g, result, result.firstKey())) {
            throw new IndexOutOfBoundsException("Solução inexistente");
        }

        return result;
    }

    /**
     * Recursive method for coloring map
     *
     * @param g           - graph containing vertices and edges
     * @param coloringMap - map containing vertices and respective colors
     * @param v           - vertex to color
     * @param <V>
     * @param <E>
     * @return boolean to enter/exit recursive loop
     */
    public static <V, E> boolean graphColoringUtil(Graph<V, E> g, TreeMap<V, Integer> coloringMap, V v) {
        /* base case: If all vertices are assigned
           a color then return true */
        if (!coloringMap.containsValue(99))
            return true;

        /* Consider this vertex v and try different
           colors */
        for (int c = 0; c < 4; c++) {
            /* Check if assignment of color c to v
               is fine*/
            if (isSafe(v, coloringMap, c, g)) {
                coloringMap.replace(v, c);

                /* recur to assign colors to rest
                   of the vertices */
                if (graphColoringUtil(g, coloringMap, coloringMap.higherKey(v)))
                    return true;

                /* If assigning color c doesn't lead
                   to a solution then remove it */
                coloringMap.replace(v, 99);
            }
        }

        /* If no color can be assigned to this vertex
           then return false */
        return false;
    }


    /**
     * A utility function to check if the current
     * color assignment is safe for vertex v
     *
     * @param a           coloring vertex
     * @param coloringMap map with colors
     * @param color       intended color
     * @param <V>
     * @param <E>
     * @return boolean
     */
    public static <V, E> boolean isSafe(V a, TreeMap<V, Integer> coloringMap, int color, Graph<V, E> g) {
        //check than no color is above 4 (chromatic number)
        if (color > 3)
            return false;

        //check that no adjacent vertex is colored with the same color as the color in testing
        for (Object x : g.outgoingEdges(a)) {
            Object temp;
            temp = ((Edge) x).getVDest();
            if (coloringMap.get(temp).equals(color))
                return false;
        }
        return true;
    }
//endregion

//region Ex3

    /**
     * calculates total length of a path
     *
     * @param g    graph where path is located
     * @param path path to analyze
     * @param <V>
     * @param <E>
     * @return double containing length of path
     */
    private static <V, E> double pathDistance(Graph<V, E> g, LinkedList<V> path) {
        double distance = 0;
        V before = path.get(0);
        for (int i = 1; i < path.size(); i++) {
            V current = path.get(i);
            distance += g.getEdge(before, current).getWeight();
            before = current;
        }
        return distance;
    }

    /**
     * From a set of items and it's size, calculate all possible combinations of said items
     * @param items set of items to 'shuffle'
     * @param currentPerm initial stack
     * @param permutations list containing all possible permutations
     * @param size size of stack
     */
    private static void permutations(Set<Integer> items, Stack<Integer> currentPerm, ArrayList<Stack<Integer>> permutations, int size) {

        /* permutation stack has become equal to size that we require */
        if (currentPerm.size() == size) {
            permutations.add((Stack<Integer>) currentPerm.clone());
        }

        /* items available for permutation */
        Integer[] availableItems = items.toArray(new Integer[0]);
        for (Integer i : availableItems) {
            /* add current item */
            currentPerm.push(i);

            /* remove item from available item set */
            items.remove(i);

            /* pass it on for next permutation */
            permutations(items, currentPerm, permutations, size);

            /* pop and put the removed item back */
            items.add(currentPerm.pop());
        }
    }

    /**
     * EX 3
     * Finds the shortest path between two given countries through the
     * information present in the given graph. Prints the distance of the path
     * and all the crossed country capitals
     *
     * @param map        graph with all the countries and borders
     * @param contOrigin starting country of the path, as a Country object
     * @param contDest   final country of the path, as a Country object
     * @return distance in km's between the two countries
     */
    public static double shortestPathBetweenTwoCountries(Graph map, Country contOrigin, Country contDest) {
        Graph<Country, Border> clone = map.clone();
        LinkedList<Country> path = new LinkedList<>();
        LinkedList<String> pathCapitals = new LinkedList<>();
        double distance = GraphAlgorithms.shortestPath(clone, contOrigin, contDest, path);
        pathCapitals = getPathCapitals(path);
        System.out.println("Distance between the two countries: " + distance);
        System.out.print("Crossed capitals': ");
        for (String cap : pathCapitals) {
            System.out.print(cap + "  ");
        }
        return distance;
    }

    /**
     * Given a LinkedList with Country objects, extracts the capital String from
     * each one and returns them in a LinkedList structure. Used to gather the
     * crossed capitals' of a path between two countries
     *
     * @param path LinkedList with the countries crossed in the path, as Country
     *             objects
     * @return LinkedList with the crossed capitals, as Strings
     */
    private static LinkedList<String> getPathCapitals(LinkedList<Country> path) {
        LinkedList<String> caps = new LinkedList<>();
        path.forEach((temp) -> {
            caps.add(temp.getCapital());
        });
        return caps;
    }

    //endregion

//region Ex4

    /**
     * EX 4
     * Finds the shortest path between two countries, being given the start,
     * stop and intermediate countries for the path, through the information
     * present in the given graph.
     *
     * @param <V>         Graph Vertex
     * @param <E>         Graph Edge
     * @param g           graph with the map information
     * @param origin      starting country of the path
     * @param destination final country of the path
     * @param stops       necessary intermediate stops of the path
     * @param resultPath  LinkedList structure to store the result path
     * @return distance between the origin and destination countries, in km
     */
    public static <V, E> double shortestPathWithStops(Graph g, V origin, V destination, V[] stops, LinkedList<V> resultPath) {
        if (!g.validVertex(origin) && !g.validVertex(destination)) {
            return 0;
        } else if (stops == null) {
            return shortestPath(g, origin, destination, resultPath);
        } else {
            for (V stop : stops) {
                if (!g.validVertex(stop)) {
                    return 0;
                }
            }
        }

        LinkedList lp = new LinkedList();
        lp.add(origin);
        for (V stop : stops) {
            lp.add(stop);
        }
        lp.add(destination);

        Set<Integer> f = new HashSet<>();
        for (int i = 1; i < lp.size() - 1; i++) {
            f.add(i);
        }
        Stack<Integer> l = new Stack<>();
        ArrayList<Stack<Integer>> permutations = new ArrayList<>();
        permutations(f, l, permutations, lp.size() - 2);

        double[][] adjMatrix = floydWarshall(g);
        double min = Double.MAX_VALUE;
        Stack minPath = null;
        for (Stack path : permutations) {
            double distancia = 0;

            path.insertElementAt(0, 0);
            path.push(path.size());

            for (int j = 0; j < path.size() - 1; j++) {
                distancia += adjMatrix[g.getKey((V) lp.toArray()[(int) path.get(j)])][g.getKey((V) lp.toArray()[(int) path.get(j + 1)])];
            }
            if (distancia < min) {
                min = distancia;
                minPath = (Stack) path.clone();
            }
        }

        int before = (int) minPath.pop();
        while (!minPath.isEmpty()) {
            int stop = (int) minPath.pop();
            V vBefore = (V) lp.get(before);
            V vStop = (V) lp.get(stop);
            LinkedList<V> currentPath = new LinkedList<>();
            shortestPath(g, vBefore, vStop, currentPath);
            for (V vertex : currentPath) {
                resultPath.add(vertex);
            }
            resultPath.removeLast();
            before = stop;
        }
        resultPath.add((V) lp.getFirst());
        return min;
    }

    /**
     * translates Graph g to a adjacency matrix and applies Floyd-Warshall algorithm to it
     * @param g graph to translate
     * @param <V>
     * @param <E>
     * @return double matrix with FloydWarshall applied to g's adjacency matrix
     */
    private static <V, E> double[][] floydWarshall(Graph<V, E> g) {
        double[][] adjacency = new double[g.numVertices()][g.numVertices()];
        for (int i = 0; i < g.numVertices(); i++) {
            for (int j = 0; j < g.numVertices(); j++) {
                adjacency[i][j] = Double.MAX_VALUE;
            }
        }

        for (V vertex : g.vertices()) {
            int x = g.getKey(vertex);
            adjacency[x][x] = 0;
        }

        for (Object edge : g.edges()) {
            int x = g.getKey((V) ((Edge) edge).getVOrig());
            int y = g.getKey((V) ((Edge) edge).getVDest());
            adjacency[x][y] = ((Edge) edge).getWeight();
        }

        for (int k = 0; k < g.numVertices(); k++) {
            for (int i = 0; i < g.numVertices(); i++) {
                for (int j = 0; j < g.numVertices(); j++) {
                    if (adjacency[i][j] > adjacency[i][k] + adjacency[k][j]) {
                        adjacency[i][j] = adjacency[i][k] + adjacency[k][j];
                    }
                }
            }
        }
        return adjacency;
    }

//endregion

//region Ex5

    /**
     * returns largest possible cycle starting from vert V length
     *
     * @param <V>
     * @param <E>
     * @param g          graph containing vertices and edges
     * @param vert       vertex to start and end the cycle
     * @return double containing largest cycle computed distance
     */
    public static <V, E> LinkedList<V> largestCycleWithStart(Graph<V, E> g, V vert) {
        ArrayList<LinkedList<V>> result = new ArrayList<>();
        LinkedList path = new LinkedList();
        TreeMap<V, Boolean> visited = new TreeMap<>();

        for (V a : g.vertices()) {
            visited.putIfAbsent(a, false);
        }
        backtrackingCycle(g, vert, vert, result, path, visited);

        int cycleMaxSize = 0;
        LinkedList<V> maxCycle = null;
        double minCycleDistance = Double.MAX_VALUE;
        for (LinkedList<V> cycle : result) {
            if (cycle.size() > cycleMaxSize || (cycle.size() == cycleMaxSize && pathDistance(g, cycle) < minCycleDistance)) {
                cycleMaxSize = cycle.size();
                maxCycle = (LinkedList<V>) cycle.clone();
                minCycleDistance = pathDistance(g, cycle);
            }
        }
        return maxCycle;
    }


    /**
     * Recursive backtracking algorithm, based on bfs applying nnh and cycle detection
     *
     * @param g       graph to search in
     * @param vert    Current vertex to analyze
     * @param orig    Origin vertex for cyce detection
     * @param cycles  known cycles
     * @param path    current path
     * @param visited list of visited vertices
     * @param <V>
     * @param <E>
     */
    private static <V, E> void backtrackingCycle(Graph<V, E> g, V vert, V orig, ArrayList<LinkedList<V>> cycles, LinkedList<V> path, TreeMap<V, Boolean> visited) {
        Comparator<V> c = (Comparator<V>) Comparator.comparing(p -> g.getEdge(vert, (V) p).getWeight()).thenComparing(p -> p.toString());
        path.push(vert);
        List adjVerts = new ArrayList((Collection) g.adjVertices(vert));
        int total = 0;
        Collections.sort(adjVerts, c);
        for (Object vAdj : adjVerts) {
            if (vAdj.equals(orig) && path.size() > 2) {
                path.push(orig);
                cycles.add(new LinkedList<>(path));
                path.pop();
                for (V a : g.vertices()) {
                    visited.replace(a, false);
                }
            } else if (!path.contains(vAdj) && !visited.get(vAdj)) {
                backtrackingCycle(g, (V) vAdj, orig, cycles, path, visited);
            }
        }

        visited.replace(vert, true);
        path.pop();
    }

//endregion

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g    Graph instance
     * @param vert information of the Vertex that will be the source of the search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert))
            return null;
        LinkedList<V> qAux = new LinkedList<>();
        LinkedList<V> qbfs = new LinkedList<>();

        qAux.add(vert);
        qbfs.add(vert);

        while (!qAux.isEmpty()) {
            vert = qAux.remove();
            for (V vAdj : g.adjVertices(vert)) {
                if (!qbfs.contains(vert)) {
                    qAux.add(vAdj);
                    qbfs.add(vAdj);
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g     Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param qdfs  queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, LinkedList<V> qdfs) {
        qdfs.add(vOrig);
        for (V vAdj : g.adjVertices(vOrig)) {
            if (!qdfs.contains(vAdj))
                DepthFirstSearch(g, vAdj, qdfs);
        }
    }

    /**
     * @param g    Graph instance
     * @param vert information of the Vertex that will be the source of the search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert))
            return null;
        LinkedList<V> qdfs = new LinkedList<>();
        DepthFirstSearch(g, vert, qdfs);
        return qdfs;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g     Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param path  stack with vertices of the current path (the path is in reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest,
                                        LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        path.push(vOrig);
        for (V vAdj : g.adjVertices(vOrig)) {
            if (vAdj.equals(vDest)) {
                path.push(vDest);
                paths.add(new LinkedList<>(path));
                path.pop();
            } else if (!path.contains(vAdj)) {
                allPaths(g, vAdj, vDest, path, paths);
            }
        }
        path.pop();
    }

    /**
     * @param g     Graph instance
     * @param vOrig information of the Vertex origin
     * @param vDest information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<LinkedList<V>>();
        if (g.validVertex(vOrig) && g.validVertex(vDest)) {
            allPaths(g, vOrig, vDest, path, paths);
        }
        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights
     * This implementation uses Dijkstra's algorithm
     *
     * @param g        Graph instance
     * @param vOrig    Vertex that will be the source of the path
     * @param visited  set of discovered vertices
     * @param pathKeys minimum path vertices keys
     * @param dist     minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
                                                  boolean[] visited, int[] pathKeys, double[] dist) {
        int vKey = g.getKey(vOrig);
        dist[vKey] = 0;

        while (vKey != -1) {
            vOrig = vertices[vKey];
            visited[vKey] = true;

            for (V vAdj : g.adjVertices(vOrig)) {
                int vKAdj = g.getKey(vAdj);
                Edge<V, E> edge = g.getEdge(vOrig, vAdj);
                if (!visited[vKAdj] && dist[vKAdj] > dist[vKey] + edge.getWeight()) {
                    dist[vKAdj] = dist[vKey] + edge.getWeight();
                    pathKeys[vKAdj] = vKey;
                }
            }
            double mindst = Double.MAX_VALUE;
            vKey = -1;

            for (int i = 0; i < g.numVertices(); i++) {
                if (!visited[i] && dist[i] < mindst) {
                    mindst = dist[i];
                    vKey = i;
                }
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf
     * The path is constructed from the end to the beginning
     *
     * @param g        Graph instance
     * @param vOrig    information of the Vertex origin
     * @param vDest    information of the Vertex destination
     * @param pathKeys minimum path vertices keys
     * @param path     stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {
        // atencao pode dar erro
        if (!vOrig.equals(vDest)) {
            path.push(vDest);
            int prvKey = pathKeys[g.getKey(vDest)];
            vDest = verts[prvKey];
            getPath(g, vOrig, vDest, verts, pathKeys, path);
        } else
            path.push(vOrig);
    }

    //shortest-path between vOrig and vDest
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if (!g.validVertex(vOrig) || !g.validVertex(vDest))
            return 0;

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, g.allkeyVerts(), visited, pathKeys, dist);

        // double lengthPath = dist[vDest.getKey()];
        double lengthPath = dist[g.getKey(vDest)];

        if (lengthPath != Double.MAX_VALUE) {
            getPath(g, vOrig, vDest, g.allkeyVerts(), pathKeys, shortPath);
            return lengthPath;
        }
        return 0;
    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V
            vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {

        if (!g.validVertex(vOrig)) return false;

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        dists.clear();
        paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList<V> shortPath = new LinkedList<>();
            if (dist[i] != Double.MAX_VALUE)
                getPath(g, vOrig, vertices[i], vertices, pathKeys, shortPath);
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty())
            pathrev.push(pathcopy.pop());

        return pathrev;
    }

}